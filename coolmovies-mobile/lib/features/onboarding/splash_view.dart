import 'package:coolmovies/core/navigation/app_routes.dart';
import 'package:coolmovies/core/navigation/navigation_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';

class SplashView extends StatefulWidget {
  const SplashView({super.key});

  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  void initState() {
    Future.delayed(const Duration(seconds: 3), () async {
      pushToAndClearStack(context, AppRoutes.moviesListView.path);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: const Text(
          'Cool Movies',
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ).animate().fade(duration: 750.ms).scale(delay: 750.ms),
      ),
    );
  }
}
