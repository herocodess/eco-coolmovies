part of 'movies_bloc.dart';

@immutable
abstract class MoviesEvent {}

class FetchMoviesEvent extends MoviesEvent {}

class FetchAllMovieReviewsEvent extends MoviesEvent {}

class FetchMovieReviewsByMovieIdEvent extends MoviesEvent {
  FetchMovieReviewsByMovieIdEvent({required this.movieId});
  final String movieId;
}

class CreateMovieReviewEvent extends MoviesEvent {
  CreateMovieReviewEvent({required this.dto});
  final CreateMovieReviewDTO dto;
}
