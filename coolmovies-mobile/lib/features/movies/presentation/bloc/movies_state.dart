part of 'movies_bloc.dart';

@immutable
abstract class MoviesState {}

class MoviesInitial extends MoviesState {}

class FetchMoviesLoadingState extends MoviesState {}

class FetchMoviesSuccessState extends MoviesState {
  FetchMoviesSuccessState({required this.movies});
  final List<MoviesModel> movies;
}

class FetchMoviesFailureState extends MoviesState {
  FetchMoviesFailureState({required this.error});
  final String error;
}

class FetchAllMovieReviewsLoadingState extends MoviesState {}

class FetchAllMovieReviewsSuccessState extends MoviesState {
  FetchAllMovieReviewsSuccessState({required this.movieReviews});
  final List<AllMovieReviewsModel> movieReviews;
}

class FetchAllMovieReviewsFailureState extends MoviesState {
  FetchAllMovieReviewsFailureState({required this.error});
  final String error;
}

class FetchMovieReviewsByMovieIdLoadingState extends MoviesState {}

class FetchMovieReviewsByMovieIdSuccessState extends MoviesState {
  FetchMovieReviewsByMovieIdSuccessState({required this.movieReviews});
  final List<MovieReviewsModel> movieReviews;
}

class FetchMovieReviewsByMovieIdFailureState extends MoviesState {
  FetchMovieReviewsByMovieIdFailureState({required this.error});
  final String error;
}

class CreateMovieReviewLoadingState extends MoviesState {}

class CreateMovieReviewSuccessState extends MoviesState {
  CreateMovieReviewSuccessState({required this.data});
  final MovieReviewsModel data;
}

class CreateMovieReviewFailureState extends MoviesState {
  CreateMovieReviewFailureState({required this.error});
  final String error;
}
