// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:coolmovies/core/dto/create_movie_review_dto.dart';
import 'package:coolmovies/core/exceptions/dio_exceptions.dart';
import 'package:coolmovies/core/managers/app_manager.dart';
import 'package:coolmovies/core/models/all_movie_reviews_model.dart';
import 'package:coolmovies/core/models/movie_reviews_model.dart';
import 'package:coolmovies/core/models/movies_model.dart';
import 'package:coolmovies/core/utils/extensions.dart';
import 'package:coolmovies/features/movies/data/movies_repository.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

part 'movies_event.dart';
part 'movies_state.dart';

class MoviesBloc extends Bloc<MoviesEvent, MoviesState> {
  MoviesBloc()
      : _moviesRepository = locator.get<MoviesRepository>(),
        super(MoviesInitial()) {
    on<FetchMoviesEvent>((event, emit) async {
      emit(FetchMoviesLoadingState());
      try {
        final response = await _moviesRepository.fetchLocalMovies();
        if (response.isRight()) {
          emit(FetchMoviesSuccessState(
            movies: response.right,
          ));
        }

        final res = await _moviesRepository.fetchMovies();
        res.fold((l) => emit(FetchMoviesFailureState(error: l)), (r) {
          return emit(
            FetchMoviesSuccessState(
              movies: r,
            ),
          );
        });
      } on DioException catch (e) {
        final ex = DioExceptions.getDioException(e);
        emit(FetchMoviesFailureState(error: ex));
      }
    });
    on<FetchAllMovieReviewsEvent>((event, emit) async {
      emit(FetchAllMovieReviewsLoadingState());
      try {
        final response = await _moviesRepository.fetchAllMovieReviews();
        response.fold((l) => emit(FetchAllMovieReviewsFailureState(error: l)),
            (r) {
          return emit(
            FetchAllMovieReviewsSuccessState(
              movieReviews: r,
            ),
          );
        });
      } on DioException catch (e) {
        final ex = DioExceptions.getDioException(e);
        emit(FetchAllMovieReviewsFailureState(error: ex));
      }
    });
    on<FetchMovieReviewsByMovieIdEvent>((event, emit) async {
      emit(FetchMovieReviewsByMovieIdLoadingState());
      try {
        final response =
            await _moviesRepository.fetchMovieReviewsByMovieId(event.movieId);
        response.fold(
            (l) => emit(FetchMovieReviewsByMovieIdFailureState(error: l)), (r) {
          return emit(
            FetchMovieReviewsByMovieIdSuccessState(
              movieReviews: r,
            ),
          );
        });
      } on DioException catch (e) {
        final ex = DioExceptions.getDioException(e);
        emit(FetchMovieReviewsByMovieIdFailureState(error: ex));
      }
    });
    on<CreateMovieReviewEvent>((event, emit) async {
      emit(CreateMovieReviewLoadingState());
      try {
        final response = await _moviesRepository.createMovieReview(event.dto);
        response.fold((l) => emit(CreateMovieReviewFailureState(error: l)),
            (r) {
          return emit(
            CreateMovieReviewSuccessState(
              data: r,
            ),
          );
        });
      } on DioException catch (e) {
        final ex = DioExceptions.getDioException(e);
        emit(CreateMovieReviewFailureState(error: ex));
      }
    });
  }

  final MoviesRepository _moviesRepository;
}
