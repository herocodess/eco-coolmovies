import 'package:coolmovies/core/dto/create_movie_review_dto.dart';
import 'package:coolmovies/core/models/movies_model.dart';
import 'package:coolmovies/core/navigation/navigation_service.dart';
import 'package:coolmovies/core/utils/colors.dart';
import 'package:coolmovies/core/utils/extensions.dart';
import 'package:coolmovies/features/movies/presentation/bloc/movies_bloc.dart';
import 'package:coolmovies/widgets/custom_alerts.dart';
import 'package:coolmovies/widgets/custom_button.dart';
import 'package:coolmovies/widgets/custom_input_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class CreateMovieReviewView extends HookWidget {
  final MoviesModel? movie;
  const CreateMovieReviewView({super.key, required this.movie});

  @override
  Widget build(BuildContext context) {
    final reviewTitleController = useTextEditingController();
    final reviewBodyController = useTextEditingController();
    final initialRating = useState<double>(0.0);
    final isLoading = useState<bool>(false);

    final movieBloc = BlocProvider.of<MoviesBloc>(context);

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        centerTitle: true,
        title: const Text(
          'Add Movie Review',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            IgnorePointer(
              ignoring: isLoading.value,
              child: InputFields(
                  label: 'Title',
                  hint: 'Review Title',
                  controller: reviewTitleController),
            ),
            SizedBox(
              height: context.height * 0.02,
            ),
            IgnorePointer(
              ignoring: isLoading.value,
              child: InputFields(
                label: 'Body',
                hint: 'Review Body',
                controller: reviewBodyController,
                maxLines: 4,
              ),
            ),
            SizedBox(
              height: context.height * 0.02,
            ),
            Text(
              'Rating',
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: AppColors.blackColor.withOpacity(0.8),
              ),
            ),
            SizedBox(
              height: context.height * 0.005,
            ),
            IgnorePointer(
              ignoring: isLoading.value,
              child: RatingBar(
                  initialRating: initialRating.value,
                  direction: Axis.horizontal,
                  allowHalfRating: false,
                  itemCount: 5,
                  ratingWidget: RatingWidget(
                      full: const Icon(Icons.star, color: Colors.orange),
                      half: const Icon(
                        Icons.star_half,
                        color: Colors.orange,
                      ),
                      empty: const Icon(
                        Icons.star_outline,
                        color: Colors.orange,
                      )),
                  onRatingUpdate: (value) {
                    initialRating.value = value;
                  }),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BlocListener<MoviesBloc, MoviesState>(
          bloc: movieBloc,
          listener: (context, state) {
            if (state is CreateMovieReviewLoadingState) {
              isLoading.value = true;
            }

            if (state is CreateMovieReviewFailureState) {
              isLoading.value = false;
              Snackbars.show(state.error, isError: true, context);
            }

            if (state is CreateMovieReviewSuccessState) {
              isLoading.value = false;
              movieBloc.add(
                  FetchMovieReviewsByMovieIdEvent(movieId: movie?.id ?? ''));
              pop(context);
            }
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: PrimaryButton(
              onTap: isLoading.value
                  ? null
                  : () {
                      final dto = CreateMovieReviewDTO(
                          title: reviewTitleController.text.removeAllSpaces(),
                          body: reviewBodyController.text.removeAllSpaces(),
                          rating: int.parse(initialRating.value.toString()),
                          movieId: movie?.id ?? '',
                          userReviewerId: movie?.userCreatorId ?? '');
                      movieBloc.add(CreateMovieReviewEvent(dto: dto));
                    },
              text: 'Add Movie Review',
              buttonColor: AppColors.blackColor,
              isLoading: isLoading.value,
            ),
          )),
    );
  }
}
