import 'package:cached_network_image/cached_network_image.dart';
import 'package:coolmovies/core/models/movie_reviews_model.dart';
import 'package:coolmovies/core/models/users_movie_directors_model.dart';
import 'package:coolmovies/core/navigation/app_routes.dart';
import 'package:coolmovies/core/navigation/navigation_service.dart';
import 'package:coolmovies/core/utils/extensions.dart';
import 'package:coolmovies/core/models/movies_model.dart';
import 'package:coolmovies/core/utils/helpers.dart';
import 'package:coolmovies/core/wrappers/user_profile_bloc_wrapper.dart';
import 'package:coolmovies/features/movies/presentation/bloc/movies_bloc.dart';
import 'package:coolmovies/features/movies/presentation/widgets/review_item_widget.dart';
import 'package:coolmovies/features/users/presentation/bloc/users_bloc.dart';
import 'package:coolmovies/widgets/error_retry_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class MovieDetailView extends HookWidget {
  final MoviesModel? movie;
  const MovieDetailView({super.key, required this.movie});

  @override
  Widget build(BuildContext context) {
    final usersBloc = BlocProvider.of<UsersBloc>(context);
    final moviesBloc = BlocProvider.of<MoviesBloc>(context);
    final movieReviews = useState<List<MovieReviewsModel>>([]);

    useEffect(() {
      usersBloc
        ..add(FetchAllUsersEvent())
        ..add(FetchAllMovieDirectorsEvent());
      return null;
    }, [usersBloc, movie]);

    useEffect(() {
      moviesBloc.add(FetchMovieReviewsByMovieIdEvent(movieId: movie?.id ?? ''));
      return null;
    }, [moviesBloc, movie]);

    movieReviews.value = Helpers.filterOfflineReviews(movie?.id ?? '');
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        centerTitle: true,
        title: const Text(
          'Movie Detail',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          pushTo(context, AppRoutes.createMovieReviewView.path, extra: movie);
        },
        child: const Icon(Icons.add),
      ),
      body: UserProfileBlocWrapper(
        usersBloc: usersBloc,
        onRetry: () {
          usersBloc
            ..add(FetchAllUsersEvent())
            ..add(FetchAllMovieDirectorsEvent());
        },
        child: Builder(builder: (context) {
          UsersOrMovieDirectorsModel? movieDirectorDetails =
              Helpers.getMovieDirectorDetails(movie?.movieDirectorId ?? '',
                  context.modelWatch.moveiDirectors);

          UsersOrMovieDirectorsModel? userCreatorDetails =
              Helpers.getMovieDirectorDetails(
                  movie?.userCreatorId ?? '', context.modelWatch.users);
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: context.width,
                  height: context.height * 0.2,
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(20),
                    ),
                    image: DecorationImage(
                      image: CachedNetworkImageProvider(
                        movie?.imgUrl ?? '',
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(
                  height: context.height * 0.01,
                ),
                Text(
                  movie?.title ?? '',
                  style: const TextStyle(
                      fontSize: 22, fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: context.height * 0.03,
                ),
                const Text(
                  'Movie Director Details:',
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: context.height * 0.01,
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Expanded(
                      child: Text(
                        movieDirectorDetails?.name ?? '',
                      ),
                    ),
                    const Spacer(),
                    Expanded(
                      child: Text(
                        '${movieDirectorDetails?.age?.toString()} y/o',
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: context.height * 0.03,
                ),
                userCreatorDetails == null
                    ? const SizedBox.shrink()
                    : const Text(
                        'User Creator Details:',
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w500),
                      ),
                userCreatorDetails == null
                    ? const SizedBox.shrink()
                    : SizedBox(
                        height: context.height * 0.01,
                      ),
                userCreatorDetails == null
                    ? const SizedBox.shrink()
                    : Text(
                        userCreatorDetails.name,
                      ),
                userCreatorDetails == null
                    ? const SizedBox.shrink()
                    : SizedBox(
                        height: context.height * 0.02,
                      ),
                const Text(
                  'Reviews',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: context.height * 0.02,
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        BlocConsumer<MoviesBloc, MoviesState>(
                          bloc: moviesBloc,
                          listener: (context, state) {
                            if (state
                                is FetchMovieReviewsByMovieIdSuccessState) {
                              movieReviews.value = state.movieReviews;
                            }
                          },
                          builder: (context, state) {
                            if (state
                                    is FetchMovieReviewsByMovieIdFailureState &&
                                state.error.contains('Network')) {
                              return SizedBox(
                                height: context.height * 0.12,
                                child: ErrorRetryWidget(
                                  error: state.error,
                                  onRetry: () {
                                    moviesBloc.add(
                                      FetchMovieReviewsByMovieIdEvent(
                                          movieId: movie?.id ?? ''),
                                    );
                                  },
                                ),
                              );
                            }
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ...List.generate(
                                  movieReviews.value.length,
                                  (index) {
                                    final review = movieReviews.value[index];
                                    return ReviewItemWidget(review: review);
                                  },
                                ),
                              ],
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        }),
      ),
    );
  }
}
