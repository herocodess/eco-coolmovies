import 'package:coolmovies/core/models/movies_model.dart';
import 'package:coolmovies/core/utils/extensions.dart';
import 'package:coolmovies/features/movies/presentation/bloc/movies_bloc.dart';
import 'package:coolmovies/features/movies/presentation/widgets/movies_list_item_widget.dart';
import 'package:coolmovies/widgets/custom_shimmer.dart';
import 'package:coolmovies/widgets/custom_switcher.dart';
import 'package:coolmovies/widgets/error_retry_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class MoviesListView extends HookWidget {
  const MoviesListView({super.key});

  @override
  Widget build(BuildContext context) {
    final moviesBloc = BlocProvider.of<MoviesBloc>(context);
    final moviesList = useState<List<MoviesModel>?>(null);
    final isLoading = useState<bool>(false);

    useEffect(
      () {
        moviesBloc
          ..add(FetchMoviesEvent())
          ..add(FetchAllMovieReviewsEvent());
        return null;
      },
      [moviesBloc],
    );

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: false,
        title: const Text(
          'Movies List',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: BlocConsumer<MoviesBloc, MoviesState>(
          bloc: moviesBloc,
          listener: (context, state) {
            if (state is FetchMoviesSuccessState) {
              isLoading.value = false;
              moviesList.value = state.movies;
            }
          },
          builder: (context, state) {
            if (state is FetchMoviesFailureState &&
                state.error.contains('Network')) {
              return ErrorRetryWidget(
                error: state.error,
                onRetry: () {
                  moviesBloc.add(FetchMoviesEvent());
                },
              );
            }
            if (moviesList.value?.isEmpty == true) return const SizedBox();
            return CustomSwitcher(
              state: state is! FetchMoviesLoadingState,
              secondChild: CustomShimmerLoading.moviesListItemShimmer(context),
              firstChild: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ...List.generate(
                    moviesList.value?.length ?? 0,
                    (index) {
                      final movies = moviesList.value?[index];
                      return Column(
                        children: [
                          MovieListItemWidget(movies: movies),
                          SizedBox(
                            height: context.height * 0.02,
                          ),
                        ],
                      );
                    },
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
