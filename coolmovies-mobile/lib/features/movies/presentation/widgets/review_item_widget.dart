import 'package:coolmovies/core/models/movie_reviews_model.dart';
import 'package:coolmovies/core/utils/colors.dart';
import 'package:coolmovies/core/utils/extensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:readmore/readmore.dart';

class ReviewItemWidget extends StatelessWidget {
  const ReviewItemWidget({
    super.key,
    required this.review,
  });

  final MovieReviewsModel review;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              review.title ?? '',
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            ),
            const Spacer(),
            RatingBarIndicator(
              rating: review.rating?.toDouble() ?? 0.0,
              itemBuilder: (context, index) => const Icon(
                Icons.star,
                color: AppColors.amberColor,
              ),
              itemCount: 5,
              itemSize: 20.0,
              direction: Axis.horizontal,
            ),
          ],
        ),
        SizedBox(
          height: context.height * 0.01,
        ),
        ReadMoreText(
          review.body ?? '',
          trimLines: 2,
          colorClickableText: Colors.blue,
          trimMode: TrimMode.Line,
          trimCollapsedText: 'Show more',
          trimExpandedText: 'Show less',
          moreStyle: const TextStyle(
              fontSize: 14, fontWeight: FontWeight.bold, color: Colors.blue),
          lessStyle: const TextStyle(
              fontSize: 14, fontWeight: FontWeight.bold, color: Colors.blue),
        ),
        SizedBox(
          height: context.height * 0.02,
        ),
      ],
    );
  }
}
