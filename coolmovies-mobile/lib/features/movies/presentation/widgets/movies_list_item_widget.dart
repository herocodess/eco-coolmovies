import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:coolmovies/core/navigation/app_routes.dart';
import 'package:coolmovies/core/navigation/navigation_service.dart';
import 'package:coolmovies/core/utils/colors.dart';
import 'package:coolmovies/core/utils/extensions.dart';
import 'package:coolmovies/core/models/movies_model.dart';
import 'package:flutter/material.dart';

class MovieListItemWidget extends StatelessWidget {
  const MovieListItemWidget({
    super.key,
    required this.movies,
  });

  final MoviesModel? movies;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>
          pushTo(context, AppRoutes.moviesDetailView.path, extra: movies),
      child: SizedBox(
        width: context.width,
        height: context.height * 0.2,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.2),
                borderRadius: const BorderRadius.all(
                  Radius.circular(20),
                ),
              ),
            ),
            // Blur effect
            BackdropFilter(
              filter: ImageFilter.blur(sigmaX: -0.3, sigmaY: 0.8),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.0),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(20),
                  ),
                ),
              ),
            ),
            // Cached network image
            Container(
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(20),
                ),
                image: DecorationImage(
                  image: CachedNetworkImageProvider(
                    movies?.imgUrl ?? '',
                  ),
                  fit: BoxFit.cover,
                ),
              ),
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    movies?.title ?? '',
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: AppColors.whiteColor),
                  ),
                  SizedBox(
                    height: context.height * 0.01,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const Text(
                        'Release Date :',
                        style: TextStyle(
                            fontSize: 14, color: AppColors.whiteColor),
                      ),
                      SizedBox(
                        width: context.width * 0.03,
                      ),
                      Text(
                        movies?.releaseDate ?? '',
                        style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: AppColors.whiteColor),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
