import 'package:coolmovies/core/api/abstract_response.dart';
import 'package:coolmovies/core/database/shared_preference_service.dart';
import 'package:coolmovies/core/dto/create_movie_review_dto.dart';
import 'package:coolmovies/core/exceptions/graphql_exceptions.dart';
import 'package:coolmovies/core/models/all_movie_reviews_model.dart';
import 'package:coolmovies/core/models/movie_reviews_model.dart';
import 'package:coolmovies/core/utils/strings.dart';
import 'package:coolmovies/core/utils/typedefs.dart';
import 'package:coolmovies/features/movies/data/movies_datasource.dart';
import 'package:coolmovies/core/models/movies_model.dart';
import 'package:coolmovies/features/movies/data/movies_local_datasource.dart';
import 'package:dartz/dartz.dart';

class MoviesRepository {
  MoviesRepository(this.moviesRemoteDataSource, this.moviesLocalDataSource);
  final MoviesRemoteDataSource moviesRemoteDataSource;
  final MoviesLocalDataSource moviesLocalDataSource;

  ResponseFormat<List<MoviesModel>> fetchMovies() async {
    final response = await moviesRemoteDataSource.fetchMovies();
    if (response.data != null) {
      final data = AbstractResponse.fromJson(response);
      final List<MoviesModel> list = data.data?['allMovies']['nodes']
          .map<MoviesModel>((e) => MoviesModel.fromJson(e))
          .toList();
      Preferences.setList(key: Strings.movieListKey, list: list);
      return Right(list);
    } else {
      final error = GraphQLExceptions.getGraphQLException(response.exception!);
      return Left(error);
    }
  }

  ResponseFormat<List<MoviesModel>> fetchLocalMovies() async {
    final response = await moviesLocalDataSource.fetchMovies();
    if (response.isEmpty) {
      return const Left('No data');
    } else {
      return Right(response);
    }
  }

  ResponseFormat<List<AllMovieReviewsModel>> fetchAllMovieReviews() async {
    final response = await moviesRemoteDataSource.fetchAllMovieReviews();
    if (response.data != null) {
      final data = AbstractResponse.fromJson(response);
      final list = data.data?['allMovieReviews']['nodes']
          .map<AllMovieReviewsModel>((e) => AllMovieReviewsModel.fromJson(e))
          .toList();
      Preferences.setList(key: Strings.movieReviewsKey, list: list);
      return Right(list);
    } else {
      final error = GraphQLExceptions.getGraphQLException(response.exception!);
      return Left(error);
    }
  }

  ResponseFormat<List<MovieReviewsModel>> fetchMovieReviewsByMovieId(
      String movieId) async {
    final response =
        await moviesRemoteDataSource.fetchMovieReviewsByMovieId(movieId);
    if (response.data != null) {
      final data = AbstractResponse.fromJson(response);
      final list = data.data?['allMovieReviews']['nodes']
          .map<MovieReviewsModel>((e) => MovieReviewsModel.fromJson(e))
          .toList();
      return Right(list);
    } else {
      final error = GraphQLExceptions.getGraphQLException(response.exception!);
      return Left(error);
    }
  }

  ResponseFormat<MovieReviewsModel> createMovieReview(
      CreateMovieReviewDTO dto) async {
    final response = await moviesRemoteDataSource.createMovieReview(dto);
    if (response.data != null) {
      final data = AbstractResponse.fromJson(response);
      final review = MovieReviewsModel.fromJson(
          data.data?['createMovieReview']['movieReview']);
      return Right(review);
    } else {
      final error = GraphQLExceptions.getGraphQLException(response.exception!);
      return Left(error);
    }
  }
}
