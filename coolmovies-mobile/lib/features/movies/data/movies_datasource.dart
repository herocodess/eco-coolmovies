import 'package:coolmovies/core/api/graphql_api_client.dart';
import 'package:coolmovies/core/dto/create_movie_review_dto.dart';
import 'package:coolmovies/core/graphql/graphql_mutations.dart';
import 'package:coolmovies/core/graphql/graphql_queries.dart';
import 'package:coolmovies/core/utils/typedefs.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class MoviesRemoteDataSource {
  final GraphQLClientService graphQLClientService;

  MoviesRemoteDataSource(this.graphQLClientService);

  AbstractFuture<QueryResult> fetchMovies() async {
    final query = GraphqlQueriesData.fetchAllMoviesQuery;
    final result = await graphQLClientService.query(query);
    return result;
  }

  AbstractFuture<QueryResult> fetchAllMovieReviews() async {
    final query = GraphqlQueriesData.fetchAllMovieReviews();
    final result = await graphQLClientService.query(query);
    return result;
  }

  AbstractFuture<QueryResult> fetchMovieReviewsByMovieId(String movieId) async {
    final query = GraphqlQueriesData.fetchMovieReviewByMovieIdQuery(movieId);
    final result = await graphQLClientService.query(query);
    return result;
  }

  AbstractFuture<QueryResult> createMovieReview(
      CreateMovieReviewDTO dto) async {
    final mutation = GraphQLMutations.createMovieReviewMutation(dto);
    final result = await graphQLClientService.mutate(mutation);
    return result;
  }
}
