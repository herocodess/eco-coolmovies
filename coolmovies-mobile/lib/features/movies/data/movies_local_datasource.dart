import 'package:coolmovies/core/database/shared_preference_service.dart';
import 'package:coolmovies/core/models/movies_model.dart';
import 'package:coolmovies/core/utils/strings.dart';
import 'package:coolmovies/core/utils/typedefs.dart';

class MoviesLocalDataSource {
  AbstractFuture<List<MoviesModel>> fetchMovies() async {
    final List<MoviesModel>? movies = Preferences.getList(
      key: Strings.movieListKey,
      creator: (map) {
        return MoviesModel.fromJson(map);
      },
    );

    if (movies != null && movies.isNotEmpty) {
      return movies;
    }
    return [];
  }
}
