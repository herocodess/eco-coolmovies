// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:coolmovies/core/exceptions/dio_exceptions.dart';
import 'package:coolmovies/core/managers/app_manager.dart';
import 'package:coolmovies/core/models/users_movie_directors_model.dart';
import 'package:coolmovies/features/users/data/users_repository.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

part 'users_event.dart';
part 'users_state.dart';

class UsersBloc extends Bloc<UsersEvent, UsersState> {
  UsersBloc()
      : _usersRepository = locator.get<UsersRepository>(),
        super(UsersInitial()) {
    on<FetchAllUsersEvent>((event, emit) async {
      emit(FetchAllUsersLoadingState());
      try {
        final response = await _usersRepository.fetchUsers();
        response.fold((l) => emit(FetchAllUsersFailureState(error: l)), (r) {
          return emit(
            FetchAllUsersSuccessState(
              users: r,
            ),
          );
        });
      } on DioException catch (e) {
        final ex = DioExceptions.getDioException(e);
        emit(FetchAllUsersFailureState(error: ex));
      }
    });

    on<FetchAllMovieDirectorsEvent>((event, emit) async {
      emit(FetchAllMovieDirectorsLoadingState());
      try {
        final response = await _usersRepository.fetchMovieDirectors();
        response.fold((l) => emit(FetchAllMovieDirectorsFailureState(error: l)),
            (r) {
          return emit(
            FetchAllMovieDirectorsSuccessState(
              movieDirectors: r,
            ),
          );
        });
      } on DioException catch (e) {
        final ex = DioExceptions.getDioException(e);
        emit(FetchAllMovieDirectorsFailureState(error: ex));
      }
    });
  }

  final UsersRepository _usersRepository;
}
