part of 'users_bloc.dart';

@immutable
abstract class UsersState {}

class UsersInitial extends UsersState {}

class FetchAllUsersLoadingState extends UsersState {}

class FetchAllUsersSuccessState extends UsersState {
  FetchAllUsersSuccessState({required this.users});
  final List<UsersOrMovieDirectorsModel> users;
}

class FetchAllUsersFailureState extends UsersState {
  FetchAllUsersFailureState({required this.error});
  final String error;
}

class FetchAllMovieDirectorsLoadingState extends UsersState {}

class FetchAllMovieDirectorsSuccessState extends UsersState {
  FetchAllMovieDirectorsSuccessState({required this.movieDirectors});
  final List<UsersOrMovieDirectorsModel> movieDirectors;
}

class FetchAllMovieDirectorsFailureState extends UsersState {
  FetchAllMovieDirectorsFailureState({required this.error});
  final String error;
}
