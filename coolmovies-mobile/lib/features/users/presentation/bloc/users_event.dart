part of 'users_bloc.dart';

@immutable
abstract class UsersEvent {}

class FetchAllUsersEvent extends UsersEvent {
  FetchAllUsersEvent();
}

class FetchAllMovieDirectorsEvent extends UsersEvent {
  FetchAllMovieDirectorsEvent();
}
