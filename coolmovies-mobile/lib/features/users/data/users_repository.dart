import 'package:coolmovies/core/api/abstract_response.dart';
import 'package:coolmovies/core/database/shared_preference_service.dart';
import 'package:coolmovies/core/exceptions/graphql_exceptions.dart';
import 'package:coolmovies/core/models/users_movie_directors_model.dart';
import 'package:coolmovies/core/utils/strings.dart';
import 'package:coolmovies/core/utils/typedefs.dart';
import 'package:coolmovies/features/users/data/users_datasource.dart';
import 'package:dartz/dartz.dart';

class UsersRepository {
  UsersRepository(this.usersRemoteDataSource);
  final UsersRemoteDataSource usersRemoteDataSource;

  ResponseFormat<List<UsersOrMovieDirectorsModel>> fetchMovieDirectors() async {
    final response = await usersRemoteDataSource.fetchMovieDirectors();
    if (response.data != null) {
      final data = AbstractResponse.fromJson(response);
      final list = data.data?['allMovieDirectors']['nodes']
          .map<UsersOrMovieDirectorsModel>(
              (e) => UsersOrMovieDirectorsModel.fromJson(e))
          .toList();
      Preferences.setList(key: Strings.movieDirectorsKey, list: list);
      return Right(list);
    } else {
      final error = GraphQLExceptions.getGraphQLException(response.exception!);
      return Left(error);
    }
  }

  ResponseFormat<List<UsersOrMovieDirectorsModel>> fetchUsers() async {
    final response = await usersRemoteDataSource.fetchUsers();
    if (response.data != null) {
      final data = AbstractResponse.fromJson(response);
      final list = data.data?['allUsers']['nodes']
          .map<UsersOrMovieDirectorsModel>(
              (e) => UsersOrMovieDirectorsModel.fromJson(e))
          .toList();
      Preferences.setList(key: Strings.movieUserCreatorsKey, list: list);
      return Right(list);
    } else {
      final error = GraphQLExceptions.getGraphQLException(response.exception!);
      return Left(error);
    }
  }
}
