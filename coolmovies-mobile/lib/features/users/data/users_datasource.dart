import 'package:coolmovies/core/api/graphql_api_client.dart';
import 'package:coolmovies/core/graphql/graphql_queries.dart';
import 'package:coolmovies/core/utils/typedefs.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class UsersRemoteDataSource {
  final GraphQLClientService graphQLClientService;

  UsersRemoteDataSource(this.graphQLClientService);

  AbstractFuture<QueryResult> fetchMovieDirectors() async {
    final query = GraphqlQueriesData.fetchMovieDirectorsQuery();
    final result = await graphQLClientService.query(query);
    return result;
  }

  AbstractFuture<QueryResult> fetchUsers() async {
    final query = GraphqlQueriesData.fetchUsersQuery();
    final result = await graphQLClientService.query(query);
    return result;
  }
}
