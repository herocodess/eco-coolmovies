import 'package:coolmovies/features/movies/presentation/bloc/movies_bloc.dart';
import 'package:coolmovies/features/users/presentation/bloc/users_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppBlocProviders {
  static List<BlocProvider> providers = [
    BlocProvider<UsersBloc>(
      create: (BuildContext context) => UsersBloc(),
    ),
    BlocProvider<MoviesBloc>(
      create: (BuildContext context) => MoviesBloc(),
    ),
  ];
}
