import 'package:coolmovies/app_view_model.dart';
import 'package:coolmovies/config.dart';
import 'package:coolmovies/core/api/graphql_api_client.dart';
import 'package:coolmovies/core/database/shared_preference_service.dart';
import 'package:coolmovies/core/managers/app_manager.dart';
import 'package:coolmovies/core/navigation/router.dart';
import 'package:coolmovies/core/utils/bloc_observer.dart';
import 'package:coolmovies/providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  await dotenv.load(fileName: Config.fileName);
  await AppManager.initializeDependencies();
  await Preferences.initalize();
  await locator.allReady();

  /// Calling the [AppBlocObserver] which is a concrete impl of the abstract class [BlocObserver]
  /// which is used to monitor the state changes throughout the app.
  Bloc.observer = AppBlocObserver();

  ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClientService.client,
  );

  runApp(GraphQLProvider(
    client: client,
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: AppBlocProviders.providers,
      child: OverlaySupport.global(
        child: ChangeNotifierProvider(
          create: (context) => AppViewModel(),
          child: MaterialApp.router(
            title: 'Cool Movies',
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            routerConfig: locator.get<RouterClass>().router,
            builder: (context, widget) => Stack(
              children: [
                Overlay(
                  initialEntries: [
                    OverlayEntry(
                      builder: (context) => widget ?? const SizedBox.shrink(),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
