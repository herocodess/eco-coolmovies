import 'dart:collection';

import 'package:coolmovies/core/models/users_movie_directors_model.dart';
import 'package:flutter/material.dart';

class AppViewModel extends ChangeNotifier {
  final List<UsersOrMovieDirectorsModel> _users = [];
  List<UsersOrMovieDirectorsModel> get users => UnmodifiableListView(_users);

  final List<UsersOrMovieDirectorsModel> _movieDirectors = [];
  List<UsersOrMovieDirectorsModel> get moveiDirectors =>
      UnmodifiableListView(_movieDirectors);

  void cacheUsers(List<UsersOrMovieDirectorsModel> data) {
    _users
      ..clear()
      ..addAll(data);
    notifyListeners();
  }

  void cacheMovieDirectors(List<UsersOrMovieDirectorsModel> data) {
    _movieDirectors
      ..clear()
      ..addAll(data);
    notifyListeners();
  }
}
