import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class Config {
  static String get fileName =>
      kDebugMode ? '.env.development' : '.env.production';
  static String get androidBaseUrl => dotenv.env['AND_BASE_URL'] ?? '';
  static String get iosBaseUrl => dotenv.env['IOS_BASE_URL'] ?? '';
}
