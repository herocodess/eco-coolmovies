import 'package:coolmovies/core/utils/extensions.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class CustomShimmer extends StatelessWidget {
  const CustomShimmer({
    super.key,
    this.height,
    this.width,
    this.borderRadius,
    this.baseColor,
    this.highlightColor,
    this.backgroundColor,
  });
  final double? height, width;
  final BorderRadiusGeometry? borderRadius;
  final Color? baseColor, highlightColor, backgroundColor;

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: baseColor ?? Colors.grey.withOpacity(.25),
      highlightColor: highlightColor ?? Colors.white54,
      child: Container(
        height: height ?? 35,
        margin: const EdgeInsets.only(top: 8),
        width: width ?? 150,
        decoration: BoxDecoration(
          color: backgroundColor ?? Colors.grey.withOpacity(.7),
          borderRadius: borderRadius ?? BorderRadius.circular(5),
        ),
      ),
    );
  }
}

class CustomShimmerLoading {
  static Widget moviesListItemShimmer(BuildContext context) {
    return Column(
      children: [
        ...List.generate(
          3,
          (index) => Column(
            children: [
              Container(
                width: context.width,
                height: context.height * 0.2,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                ),
                child: const CustomShimmer(),
              ),
              SizedBox(
                height: context.height * 0.01,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
