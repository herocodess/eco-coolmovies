import 'package:coolmovies/core/utils/extensions.dart';
import 'package:coolmovies/widgets/custom_button.dart';
import 'package:flutter/cupertino.dart';

class ErrorRetryWidget extends StatelessWidget {
  const ErrorRetryWidget({
    super.key,
    required this.error,
    required this.onRetry,
    this.actionText,
  });

  final String error;
  final Function() onRetry;
  final String? actionText;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            error,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
            ),
          ),
          SizedBox(
            height: context.height * 0.02,
          ),
          PrimaryButton(onTap: onRetry, text: actionText ?? 'Retry'),
        ],
      ),
    );
  }
}
