import 'package:coolmovies/core/database/shared_preference_service.dart';

class MoviesModel extends Cachable {
  String? id;
  String? title;
  String? movieDirectorId;
  String? userCreatorId;
  String? releaseDate;
  String? imgUrl;

  MoviesModel(
      {this.id,
      this.title,
      this.movieDirectorId,
      this.userCreatorId,
      this.releaseDate,
      this.imgUrl});

  MoviesModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    movieDirectorId = json['movieDirectorId'];
    userCreatorId = json['userCreatorId'];
    releaseDate = json['releaseDate'];
    imgUrl = json['imgUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['movieDirectorId'] = movieDirectorId;
    data['userCreatorId'] = userCreatorId;
    data['releaseDate'] = releaseDate;
    data['imgUrl'] = imgUrl;
    return data;
  }

  @override
  Map<String, dynamic> toMap() {
    return toJson();
  }
}
