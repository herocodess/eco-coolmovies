import 'package:coolmovies/core/database/shared_preference_service.dart';

class UsersOrMovieDirectorsModel extends Cachable {
  final String id;
  final String name;
  final int? age;

  UsersOrMovieDirectorsModel({required this.id, required this.name, this.age});

  factory UsersOrMovieDirectorsModel.fromJson(Map<String, dynamic> json) {
    return UsersOrMovieDirectorsModel(
      id: json['id'],
      name: json['name'],
      age: json['age'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      if (age != null) 'age': age,
    };
  }

  @override
  Map<String, dynamic> toMap() {
    return toJson();
  }
}
