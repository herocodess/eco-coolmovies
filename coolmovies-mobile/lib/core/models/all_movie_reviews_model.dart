import 'package:coolmovies/core/database/shared_preference_service.dart';

class AllMovieReviewsModel extends Cachable {
  String? title;
  String? body;
  int? rating;
  MovieByMovieId? movieByMovieId;
  CommentsByMovieReviewId? commentsByMovieReviewId;

  AllMovieReviewsModel(
      {this.title,
      this.body,
      this.rating,
      this.movieByMovieId,
      this.commentsByMovieReviewId});

  AllMovieReviewsModel.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    body = json['body'];
    rating = json['rating'];
    movieByMovieId = json['movieByMovieId'] != null
        ? MovieByMovieId.fromJson(json['movieByMovieId'])
        : null;
    commentsByMovieReviewId = json['commentsByMovieReviewId'] != null
        ? CommentsByMovieReviewId.fromJson(json['commentsByMovieReviewId'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = title;
    data['body'] = body;
    data['rating'] = rating;
    if (movieByMovieId != null) {
      data['movieByMovieId'] = movieByMovieId!.toJson();
    }
    if (commentsByMovieReviewId != null) {
      data['commentsByMovieReviewId'] = commentsByMovieReviewId!.toJson();
    }
    return data;
  }

  @override
  Map<String, dynamic> toMap() {
    return toJson();
  }
}

class MovieByMovieId {
  String? id;
  String? title;
  UserByUserCreatorId? userByUserCreatorId;

  MovieByMovieId({this.id, this.title, this.userByUserCreatorId});

  MovieByMovieId.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    userByUserCreatorId = json['userByUserCreatorId'] != null
        ? UserByUserCreatorId.fromJson(json['userByUserCreatorId'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    if (userByUserCreatorId != null) {
      data['userByUserCreatorId'] = userByUserCreatorId!.toJson();
    }
    return data;
  }
}

class UserByUserCreatorId {
  String? id;
  String? name;

  UserByUserCreatorId({this.id, this.name});

  UserByUserCreatorId.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}

class CommentsByMovieReviewId {
  List<dynamic>? nodes;

  CommentsByMovieReviewId({this.nodes});

  CommentsByMovieReviewId.fromJson(Map<String, dynamic> json) {
    if (json['nodes'] != null) {
      nodes = <dynamic>[];
      json['nodes'].forEach((v) {
        nodes!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (nodes != null) {
      data['nodes'] = nodes!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
