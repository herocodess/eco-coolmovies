class MovieReviewsModel {
  String? id;
  String? title;
  String? body;
  int? rating;
  MovieByMovieId? movieByMovieId;
  UserByUserReviewerId? userByUserReviewerId;

  MovieReviewsModel(
      {this.id,
      this.title,
      this.body,
      this.rating,
      this.movieByMovieId,
      this.userByUserReviewerId});

  MovieReviewsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    body = json['body'];
    rating = json['rating'];
    movieByMovieId = json['movieByMovieId'] != null
        ? MovieByMovieId.fromJson(json['movieByMovieId'])
        : null;
    userByUserReviewerId = json['userByUserReviewerId'] != null
        ? UserByUserReviewerId.fromJson(json['userByUserReviewerId'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['body'] = body;
    data['rating'] = rating;
    if (movieByMovieId != null) {
      data['movieByMovieId'] = movieByMovieId!.toJson();
    }
    if (userByUserReviewerId != null) {
      data['userByUserReviewerId'] = userByUserReviewerId!.toJson();
    }
    return data;
  }
}

class MovieByMovieId {
  String? title;

  MovieByMovieId({this.title});

  MovieByMovieId.fromJson(Map<String, dynamic> json) {
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = title;
    return data;
  }
}

class UserByUserReviewerId {
  String? name;

  UserByUserReviewerId({this.name});

  UserByUserReviewerId.fromJson(Map<String, dynamic> json) {
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    return data;
  }
}
