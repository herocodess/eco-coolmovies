import 'package:coolmovies/core/utils/extensions.dart';
import 'package:coolmovies/features/users/presentation/bloc/users_bloc.dart';
import 'package:coolmovies/widgets/error_retry_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserProfileBlocWrapper extends StatelessWidget {
  const UserProfileBlocWrapper(
      {super.key,
      required this.usersBloc,
      required this.child,
      required this.onRetry});
  final UsersBloc usersBloc;
  final Function() onRetry;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UsersBloc, UsersState>(
      bloc: usersBloc,
      listener: (context, state) {
        if (state is FetchAllUsersSuccessState) {
          context.modelRead.cacheUsers(state.users);
        }

        if (state is FetchAllMovieDirectorsSuccessState) {
          context.modelRead.cacheMovieDirectors(state.movieDirectors);
        }
      },
      builder: (context, state) {
        if (state is FetchAllUsersFailureState &&
            state.error.contains('Network')) {
          return ErrorRetryWidget(
            error: state.error,
            onRetry: onRetry,
          );
        }

        if (state is FetchAllMovieDirectorsFailureState &&
            state.error.contains('Network')) {
          return ErrorRetryWidget(
            error: state.error,
            onRetry: onRetry,
          );
        }
        return child;
      },
    );
  }
}
