import 'package:dartz/dartz.dart';

typedef RequestParams<T> = Map<String, dynamic>;

typedef ResponseFormat<T> = Future<Either<String, T>>;

typedef AbstractFuture<T> = Future<T>;
