import 'dart:io';

import 'package:coolmovies/core/database/shared_preference_service.dart';
import 'package:coolmovies/core/models/all_movie_reviews_model.dart';
import 'package:coolmovies/core/models/movie_reviews_model.dart';
import 'package:coolmovies/core/models/users_movie_directors_model.dart';
import 'package:coolmovies/core/utils/colors.dart';
import 'package:coolmovies/core/utils/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Helpers {
  static Widget getProgressLoader(
      {double? height, double? width, Color? color}) {
    return SizedBox(
      height: (height ?? 15),
      width: (width ?? 15),
      child: Platform.isAndroid
          ? CircularProgressIndicator(
              strokeWidth: 1.5,
              color: color ?? AppColors.whiteColor,
            )
          : Transform.scale(
              scale: 0.5,
              child: CupertinoActivityIndicator(
                color: color ?? AppColors.whiteColor,
              ),
            ),
    );
  }

  static UsersOrMovieDirectorsModel? getMovieDirectorDetails(
      String movieDirectorID, List<UsersOrMovieDirectorsModel> data) {
    // Retrieve the list of movie directors from preferences
    final List<UsersOrMovieDirectorsModel>? movieDirectorsList =
        Preferences.getList(
      key: Strings.movieDirectorsKey,
      creator: (map) => UsersOrMovieDirectorsModel.fromJson(map),
    );

    // If the provided data list is empty and the retrieved list is not null
    if (data.isEmpty && movieDirectorsList != null) {
      // Search for the director in the retrieved list
      for (var mD in movieDirectorsList) {
        if (mD.id == movieDirectorID) {
          return mD;
        }
      }
    }

    // If the director was not found in the retrieved list, search in the provided list
    try {
      final director =
          data.firstWhere((element) => element.id == movieDirectorID);
      return director;
    } catch (e) {
      return null; // or handle the error as needed
    }
  }

  static UsersOrMovieDirectorsModel? getUserCreatorDetails(
      String userCreatorID, List<UsersOrMovieDirectorsModel> data) {
    // Retrieve the list of user creators from preferences
    final List<UsersOrMovieDirectorsModel>? usersCreatorsList =
        Preferences.getList(
      key: Strings.movieUserCreatorsKey,
      creator: (map) => UsersOrMovieDirectorsModel.fromJson(map),
    );

    // If the provided data list is empty and the retrieved list is not null
    if (data.isEmpty && usersCreatorsList != null) {
      // Search for the user creator in the retrieved list
      for (var userCreator in usersCreatorsList) {
        if (userCreator.id == userCreatorID) {
          return userCreator;
        }
      }
    }

    // If the user creator was not found in the retrieved list, search in the provided list
    try {
      final userCreator =
          data.firstWhere((element) => element.id == userCreatorID);
      return userCreator;
    } catch (e) {
      return null; // or handle the error as needed
    }
  }

  static List<MovieReviewsModel> filterOfflineReviews(String movieId) {
    final List<AllMovieReviewsModel>? reviews = Preferences.getList(
      key: Strings.movieReviewsKey,
      creator: (map) {
        return AllMovieReviewsModel.fromJson(map);
      },
    );

    if (reviews == null) return [];

    return reviews
        .where((review) => review.movieByMovieId?.id == movieId)
        .map((review) => MovieReviewsModel(
              title: review.title,
              body: review.body,
              rating: review.rating,
            ))
        .toList();
  }
}
