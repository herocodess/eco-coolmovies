class Strings {
  static String movieListKey = 'movie_list_key';
  static String movieReviewsKey = 'movie_reviews_key';
  static String movieDirectorsKey = 'movie_directors_key';
  static String movieUserCreatorsKey = 'movie_user_creators_key';
}
