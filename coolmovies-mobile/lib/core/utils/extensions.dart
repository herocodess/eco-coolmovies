// ignore_for_file: file_names

import 'package:coolmovies/app_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dartz/dartz.dart';

extension MediaQueryExtension on BuildContext {
  double get height => MediaQuery.sizeOf(this).height;
  double get width => MediaQuery.sizeOf(this).width;
  AppViewModel get modelRead => read<AppViewModel>();
  AppViewModel get modelWatch => watch<AppViewModel>();
}

extension EitherX<L, R> on Either<L, R> {
  R asRight() => (this as Right).value;
  L asLeft() => (this as Left).value;

  L get left => asLeft();
  R get right => asRight();
}

extension StringUtils on String {
  String removeAllSpaces() => replaceAll(' ', '');
}
