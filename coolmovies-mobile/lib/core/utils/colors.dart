import 'package:flutter/material.dart';

class AppColors {
  static const Color whiteColor = Colors.white;
  static const Color blackColor = Colors.black;
  static const Color redColor = Colors.red;
  static const Color greyColor = Colors.grey;
  static const Color amberColor = Colors.amber;
  static const Color greenColor = Colors.green;
  static const Color transparentColor = Colors.transparent;
}
