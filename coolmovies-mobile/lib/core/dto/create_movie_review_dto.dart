class CreateMovieReviewDTO {
  final String title;
  final String body;
  final int rating;
  final String movieId;
  final String userReviewerId;

  CreateMovieReviewDTO({
    required this.title,
    required this.body,
    required this.rating,
    required this.movieId,
    required this.userReviewerId,
  });

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'body': body,
      'rating': rating,
      'movieId': movieId,
      'userReviewerId': userReviewerId,
    };
  }
}
