import 'package:graphql_flutter/graphql_flutter.dart';

class AbstractResponse {
  Map<String, dynamic>? data;
  dynamic errors;

  AbstractResponse({
    required this.data,
    required this.errors,
  });

  factory AbstractResponse.fromJson(QueryResult<Object?> json) {
    if (json.hasException) {
      return AbstractResponse(data: null, errors: json.exception);
    }

    final Map<String, dynamic>? responseData = json.data;
    if (responseData == null) {
      throw 'Response data is null';
    }

    return AbstractResponse(data: responseData, errors: null);
  }

  Map<String, dynamic> toJson() {
    return {
      'data': data,
      'errors': errors,
    };
  }
}
