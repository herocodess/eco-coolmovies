import 'dart:io';

import 'package:coolmovies/config.dart';
import 'package:coolmovies/core/exceptions/dio_exceptions.dart';
import 'package:dio/dio.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class GraphQLClientService {
  static final HttpLink httpLink = HttpLink(
    Platform.isAndroid ? Config.androidBaseUrl : Config.iosBaseUrl,
  );

  static final GraphQLClient client = GraphQLClient(
    cache: GraphQLCache(store: InMemoryStore()),
    link: httpLink,
    defaultPolicies: DefaultPolicies(
      query: Policies(
        fetch: FetchPolicy.cacheAndNetwork,
      ),
      mutate: Policies(
        fetch: FetchPolicy.networkOnly,
      ),
    ),
  );

  Future<QueryResult> query(String query,
      {Map<String, dynamic>? variables}) async {
    try {
      final options = QueryOptions(
        document: gql(query),
        variables: variables ?? {},
        fetchPolicy: FetchPolicy.cacheAndNetwork,
      );

      final result = await client.query(options);

      return result;
    } on DioException catch (e) {
      throw DioExceptions.getDioException(e);
    } on SocketException catch (_) {
      throw 'No internet connection!';
    } catch (e) {
      rethrow;
    }
  }

  Future<QueryResult> mutate(String mutation,
      {Map<String, dynamic>? variables}) async {
    try {
      final options = MutationOptions(
        document: gql(mutation),
        variables: variables ?? {},
        fetchPolicy: FetchPolicy.networkOnly,
      );

      final result = await client.mutate(options);

      return result;
    } on DioException catch (e) {
      throw DioExceptions.getDioException(e);
    } on SocketException catch (_) {
      throw 'No internet connection!';
    } catch (e) {
      rethrow;
    }
  }
}
