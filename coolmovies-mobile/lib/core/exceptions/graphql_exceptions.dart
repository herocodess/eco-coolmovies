import 'package:graphql_flutter/graphql_flutter.dart';

// class GraphQLExceptions {
//   static String getGraphQLException(dynamic error) {
//     if (error is OperationException) {
//       switch (error.runtimeType) {
//         case LinkException:
//           return error.linkException!.originalException.toString();
//         default:
//           return error.graphqlErrors.first.message;
//       }
//     } else if (error is String) {
//       return error.toString();
//     } else {
//       return 'Unexpected error occurred: ${error.toString()}';
//     }
//   }
// }

class GraphQLExceptions {
  static String getGraphQLException(dynamic error) {
    if (error is OperationException) {
      switch (error.runtimeType) {
        case LinkException:
          var linkException = error.linkException;
          if (linkException is NetworkException) {
            return 'Network error: ${linkException.message}';
          }
          return linkException?.originalException.toString() ??
              'Unknown link exception';
        default:
          if (error.graphqlErrors.isNotEmpty) {
            return error.graphqlErrors.first.message;
          } else {
            return 'GraphQL operation exception occurred without specific errors';
          }
      }
    } else if (error is String) {
      return error.toString();
    } else {
      return 'Unexpected error occurred: ${error.toString()}';
    }
  }
}
