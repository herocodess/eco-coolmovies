enum AppRoutes {
  splash('splash', '/splash'),
  moviesListView('moviesListView', '/movies-list-view'),
  moviesDetailView('moviesDetailView', '/movies-detail-view'),
  createMovieReviewView('createMovieReviewView', '/create-movie-review-view'),
  ;

  const AppRoutes(this.name, this.path);
  final String name, path;
}
