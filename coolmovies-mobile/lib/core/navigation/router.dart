import 'package:coolmovies/core/navigation/app_routes.dart';
import 'package:coolmovies/core/navigation/custom_page_route.dart';
import 'package:coolmovies/core/models/movies_model.dart';
import 'package:coolmovies/features/movies/presentation/views/create_movie_review_view.dart';
import 'package:coolmovies/features/movies/presentation/views/movie_detail_view.dart';
import 'package:coolmovies/features/movies/presentation/views/movies_list_view.dart';
import 'package:coolmovies/features/onboarding/splash_view.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class RouterClass {
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  GoRouter get router {
    return GoRouter(
      initialLocation: AppRoutes.splash.path,
      navigatorKey: navigatorKey,
      routes: [
        GoRoute(
          parentNavigatorKey: navigatorKey,
          path: AppRoutes.splash.path,
          name: AppRoutes.splash.name,
          builder: (context, state) {
            return const SplashView();
          },
        ),
        GoRoute(
          parentNavigatorKey: navigatorKey,
          path: AppRoutes.moviesListView.path,
          name: AppRoutes.moviesListView.name,
          builder: (context, state) {
            return const MoviesListView();
          },
        ),
        GoRoute(
          parentNavigatorKey: navigatorKey,
          path: AppRoutes.moviesDetailView.path,
          name: AppRoutes.moviesDetailView.name,
          pageBuilder: (context, state) {
            final movie = state.extra as MoviesModel?;
            return TransparentPage(
              child: MovieDetailView(
                movie: movie,
              ),
            );
          },
        ),
        GoRoute(
          parentNavigatorKey: navigatorKey,
          path: AppRoutes.createMovieReviewView.path,
          name: AppRoutes.createMovieReviewView.name,
          pageBuilder: (context, state) {
            final movie = state.extra as MoviesModel?;
            return TransparentPage(
              child: CreateMovieReviewView(
                movie: movie,
              ),
            );
          },
        ),
      ],
    );
  }
}
