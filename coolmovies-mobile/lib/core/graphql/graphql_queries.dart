class GraphqlQueriesData {
  static String fetchAllMoviesQuery = r"""
          query AllMovies {
            allMovies {
              nodes {
                id
                imgUrl
                movieDirectorId
                userCreatorId
                title
                releaseDate
                nodeId
                userByUserCreatorId {
                  id
                  name
                  nodeId
                }
              }
            }
          }
        """;

  static String fetchMovieDirectorsQuery() {
    return '''
        query AllMoviesDirectors {
          allMovieDirectors {
            nodes {
              id
              name
              age
            }
            pageInfo {
              hasNextPage
              hasPreviousPage
            }
          }
        }
      ''';
  }

  static String fetchUsersQuery() {
    return '''
        query AllUsers {
          allUsers {
            nodes {
              id
              name
            }
            pageInfo {
              hasNextPage
              hasPreviousPage
            }
          }
        }
      ''';
  }

  static String fetchAllMovieReviews() {
    return '''
      query AllMovieReviews {
        allMovieReviews {
        nodes {
          title
          body
          rating
          movieByMovieId {
            id
            title
            userByUserCreatorId {
              id
              name
            }
          }
          commentsByMovieReviewId {
            nodes {
              id
              title
          body
          userByUserId {
            id
            name
          }
        }
      }
    }
  }
      }
    ''';
  }

  static String fetchMovieReviewByMovieIdQuery(String movieId) {
    return '''
      query AllMovieReviewsByMovieID {
          allMovieReviews(
            filter: {movieId: {equalTo: "$movieId"}}
          ) {
          nodes {
            title
            body
            rating
            movieByMovieId {
              title
            }
          }
        }
      }
    ''';
  }
}
