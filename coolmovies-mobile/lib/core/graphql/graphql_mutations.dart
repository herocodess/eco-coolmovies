import 'package:coolmovies/core/dto/create_movie_review_dto.dart';

class GraphQLMutations {
  static String createMovieReviewMutation(CreateMovieReviewDTO dto) {
    return '''
      mutation {
        createMovieReview(input: {
          movieReview: {
            title: "${dto.title}",
            body: "${dto.body}",
            rating: ${dto.rating},
            movieId: "${dto.movieId}",
            userReviewerId: "${dto.userReviewerId}"
          }
        }) {
          movieReview {
            id
            title
            body
            rating
            movieByMovieId {
              title
            }
            userByUserReviewerId {
              name
            }
          }
        }
      }
    ''';
  }
}
