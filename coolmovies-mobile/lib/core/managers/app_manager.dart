// ignore_for_file: constant_identifier_names

import 'package:coolmovies/core/api/graphql_api_client.dart';
import 'package:coolmovies/core/navigation/router.dart';
import 'package:coolmovies/features/movies/data/movies_datasource.dart';
import 'package:coolmovies/features/movies/data/movies_local_datasource.dart';
import 'package:coolmovies/features/movies/data/movies_repository.dart';
import 'package:coolmovies/features/users/data/users_datasource.dart';
import 'package:coolmovies/features/users/data/users_repository.dart';
import 'package:get_it/get_it.dart';

final GetIt locator = GetIt.instance;

const MOVIES_BOX_KEY = 'MOVIES_box##';

class AppManager {
  static Future<void> initializeDependencies() async {
    locator
      ..registerLazySingleton(() => GraphQLClientService())

      //datasources
      ..registerLazySingleton(() => MoviesRemoteDataSource(locator()))
      ..registerLazySingleton(() => MoviesLocalDataSource())
      ..registerLazySingleton(() => UsersRemoteDataSource(locator()))

      //repositories
      ..registerLazySingleton(() => MoviesRepository(locator(), locator()))
      ..registerLazySingleton(() => UsersRepository(locator()))

      //navigation
      ..registerLazySingleton(() => RouterClass());
  }
}
